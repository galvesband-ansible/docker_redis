Role Name
=========

A simple Ansible role that deploys Redis on Docker in Ubuntu 20.04.

Requirements
------------

The host should have a working Docker installation. 

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

The role itself doesn't depend on anything else but Docker is "assumed". This role is intended to be used 
in conjunction with [this docker role](https://gitlab.com/galvesband-ansible/docker).

Example Playbook
----------------

```yml
---
- name: Converge
  hosts: docker-redis-ubuntu2004
  tasks:

    - name: "Install docker"
      include_role:
        name: "docker"

    - name: "Include redis"
      include_role:
        name: "docker_redis"
        
      vars:
        
        redis_version: "6.2-alpine"
        redis_password: "so-secret"
        redis_network: redis-network
        redis_exposed_port: 36379
```

License
-------

GLP 2.0 or later.
